﻿using System;
using static System.Console;
using System.Collections.Generic;

class Program
{
    static List<string> notes = new List<string>();
    static void Main(string[] args)
    {
        bool exit = false;
        while (!exit)
        {
            WriteLine("Welcomme to notes list !");
            WriteLine("1. View notes");
            WriteLine("2. Show note");
            WriteLine("3. Add note");
            WriteLine("4. Update note");
            WriteLine("5. Delete note");
            WriteLine("6. Exit");
            WriteLine("");
            Write("Please enter your choice: ");
            int choice = Convert.ToInt32(ReadLine());

            switch (choice)
            {
                case 1:
                    ViewNotes();
                    break;

                case 2:
                    ShowNote();
                    break;

                case 3:
                    AddNote();
                    break;

                case 4:
                    UpdateNote();
                    break;

                case 5:
                    DeleteNote();
                    break;

                case 6:
                    exit = true;
                    break;

                default:
                    WriteLine("Invalid choice, Please try again");
                    break;

            }
            WriteLine();
        }
        WriteLine("Thank you for using notes list , GoodBye !");
    }
    static void ViewNotes()
    {
        if (notes.Count == 0)
        {
            WriteLine("No notes found !");
        }
        else
        {
            WriteLine("Notes");
            for (int i = 0; i < notes.Count; i++)
            {
                WriteLine($"{i + 1}. {notes[i]}");
            }
        }
    }
    static void ShowNote()
    {
        if (notes.Count == 0)
        {
            WriteLine("No notes found !");
        }
        else
        {
            WriteLine("Enter the note number to show: ");
            int noteNumber = Convert.ToInt32(ReadLine());
            if (noteNumber <= 0 || noteNumber > notes.Count)
            {
                WriteLine("Invalid note number");
            }
            else
            {
                WriteLine("Note");
                WriteLine($"{noteNumber}. {notes[noteNumber - 1]}");
            }
        }
    }
    static void AddNote()
    {
        WriteLine("Enter new note: ");
        string newNote = ReadLine();
        notes.Add(newNote);
        WriteLine("Note added successfully.");
    }
    static void UpdateNote()
    {
        WriteLine("Enter the note number to update: ");
        int noteNumber = Convert.ToInt32(ReadLine());
        if (noteNumber <= 0 || noteNumber > notes.Count)
        {
            WriteLine("Invalid note number");
        }
        else
        {
            WriteLine("Enter the update note: ");
            string updateNote = ReadLine();
            notes[noteNumber - 1] = updateNote;
            WriteLine("Note update successfuly.");
        }
    }
    static void DeleteNote()
    {
        WriteLine("Enter the note number to delete: ");
        int noteNumber = Convert.ToInt32(ReadLine());
        if (noteNumber <= 0 || noteNumber > notes.Count)
        {
            WriteLine("Invalid note number");
        }
        else
        {
            notes.RemoveAt(noteNumber - 1);
            WriteLine("Note deleted successfuly.");
        }
    }

}

